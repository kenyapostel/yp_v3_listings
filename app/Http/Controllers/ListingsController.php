<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ListingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $businesses = DB::table('business_brand')->get()->toJson();
        Storage::put('business.json', $businesses);
        dd($businesses);
    }

    /**
     *
     * Generic function to fetch businesses given the required parameters
     *
     * @param type $what_keyword ... The search phrase
     * @param type $where ... Location eg. town, county etc
     * @param type $location_id ... Location unique ID as mapped in the Reglis database, defaults to 0 if nothing is provided
     * @param type $category ... the defined business area
     * @param type $category_id ... the defined business area id
     * @param type $current_page .... paginator current page
     * @param type $per_page ... How many records to display per page
     * @param type $FilterLocality_F .... If search results will be filtered to the specified locality 0 no 1 yes
     * @return type  An associative array of the search results
     */
    function mainSearch(Request $request)
    {
        $what_keyword = $request->what_keyword;
        $where = isset($request->where) ? $request->where : "";
        $location_id = isset($request->location_id) ? $request->location_id : 0;
        $category = isset($request->category) ? $request->category : "";
        $category_id = isset($request->category_id) ? $request->category_id : 0;
        $current_page = $request->current_page;
        $per_page = $request->per_page;
        $FilterLocality_F = isset($request->FilterLocality_F) ? $request->FilterLocality_F : 0;

        $what = trim(str_replace(["limited", "ltd"], ["", ""], strtolower($what_keyword)));
        $classTitle_ID = 0;
        $lingua_id = 2; //@todo change language id to drupals default
        $count_total_results = 0;

        if ($category !== "" && $category_id === 0) {
            $classTitle_ID = $this->getCategoryIdbyName(); // import this to cms template function
        } else {
            $classTitle_ID = $category_id;
        }

        $count_total_results = $this->getCountSearchResults($lingua_id, $what, $where, $location_id, $FilterLocality_F, $classTitle_ID);
        /**
         * If no search results, omit the category and search again
         */
        if ($count_total_results <= 0 && $classTitle_ID >= 1) {
            //assign back category to zero
            $classTitle_ID = 0;
            $count_total_results = $this->getCountSearchResults($lingua_id, $what, $where, $location_id, $FilterLocality_F, $classTitle_ID);
        }

        if ($count_total_results >= 1) {
            $results = $this->getExecSearchResults($lingua_id, $what, $where, $location_id, $FilterLocality_F, $classTitle_ID, $current_page, $per_page);
            $final_results = [];
            if (sizeof($results) >= 1) :
                $final_results = ["status" => true];
                $final_results["_total_results"] = $count_total_results;
                $final_results["_cat_loc"] = $this->getCatLoc($results);
                $final_results["_search_results"] = themeSearchResults($results); // import this to cms template funtion
                return $final_results;
            else :
                return ["status" => False];
            endif;
        } else {
            return ["status" => False];
        }
    }

    /**
     * Fetch the actual results
     *
     * @param type $lingua_id
     * @param type $what
     * @param type $where
     * @param type $location_id
     * @param type $FilterLocality_F
     * @param type $classTitle_ID
     * @param type $current_page
     * @param type $per_page
     * @return type
     */
    public function getSearchResults(Request $request)
    {
        // $lingua_id = $request->lingua_id;
        // $what = $request->what;
        // $where = $request->where;
        // $location_id = $request->location_id;
        // $FilterLocality_F = $request->FilterLocality_F;
        // $classTitle_ID = $request->classTitle_ID;
        // $current_page = $request->current_page;
        // $per_page = $request->per_page;

        $lingua_id = 2;
        $what = 'wine';
        $where = 'nairobi';
        $location_id = 0;
        $FilterLocality_F = 0;
        $classTitle_ID = 0;
        $pcount = 1;
        $current_page = 0;
        $per_page = 10;

        $result = DB::connection("sqlsrv")->statement("exec sp_PAI_PESQUISA_V8 @LINGUA_ID='{$lingua_id}', @PESQUISA_F='{$what}', @LOCALITY_F='{$where}', @LOCALIDADE_ID={$location_id}, @FilterLocality_F={$FilterLocality_F}, @TITULO_ID={$classTitle_ID}, @PCOUNT={$pcount}");
        return $result;

        // $db = DB::connection('sqlsrv')->getPdo();
        // $stmt = $db->prepare("exec sp_PAI_PESQUISA_V8
        //                     'LINGUA_ID',
        //                     'PESQUISA_F',
        //                     'LOCALITY_F',
        //                     'LOCALIDADE_ID',
        //                     'FilterLocality_F' ,
        //                     'TITULO_ID',
        //                     'PINITIAL',
        //                     'PREG',
        //                     ?, ?, ?, ?, ?, ?, ?, ?
        //                     ");
        // $stmt->bindParam(1, $lingua_id, PDO::PARAM_INT);
        // $stmt->bindParam(2, $what, PDO::PARAM_STR_CHAR);
        // $stmt->bindParam(3, $where, PDO::PARAM_STR_CHAR);
        // $stmt->bindParam(4, $location_id, PDO::PARAM_INT);
        // $stmt->bindParam(5, $FilterLocality_F, PDO::PARAM_INT);
        // $stmt->bindParam(6, $classTitle_ID, PDO::PARAM_INT);
        // $stmt->bindParam(7, $current_page, PDO::PARAM_INT);
        // $stmt->bindParam(8, $per_page, PDO::PARAM_INT);
        // $stmt->execute();
        // dd($stmt->execute());


    }


    //
    function getTripAdvisorLocationId(Request $request)
    {
        $BNIC = $request->BNIC;
        $sql_triploc = "
        SELECT TOP 1 [ID_CLI_CUSTOMER_CODES]
            ,[NIC]
            ,[ID_CUSTOMER_CODES]
            ,[COD_CUSTOMER_CODES]
            ,[DESC_CUSTOMER_CODES]
            ,[CUST_CODE_SEQ]
            ,[CONTEUDO]
        FROM [NIC_CODES]
        WHERE [NIC] = :BNIC  AND [COD_CUSTOMER_CODES] = 'TripadvisorID'";
        $result = DB::connection("sqlsrv")->select($sql_triploc, array(':BNIC' => $BNIC,));
        // dd($result);
        if (!empty($result)) {
            return ["status" => true, "triplocid" => $result[0]->CONTEUDO];
        } else {
            return ["status" => false, "triplocid" => []];
        }
    }

    function getBizDetails_1(REQUEST $request){
        $lingua_id = '2';
        $nome_loc_tit_id = $request->nome_loc_tit_id;
        $sql_sp_GRA_KPD = "EXEC sp_GRA_KPD @LINGUA_ID=:lingua_id,@NOME_LOC_TIT_id=:nome_loc_tit_id";
        $results1 = DB::connection("sqlsrv")->select(
            $sql_sp_GRA_KPD,
            array(
                ':lingua_id' => $lingua_id, //language
                ':nome_loc_tit_id' => (int) (str_replace(" ", "", $nome_loc_tit_id)),
            )
        );
        return $results1;
    }

    function getBizDetails_2(REQUEST $request){
        $lingua_id = '2';
        $nome_loc_tit_id = $request->nome_loc_tit_id;
        $sql_sp_GRA_KPD = "EXEC sp_LINHAS @LINGUA_ID=:lingua_id,@NOME_LOC_TIT_id=:nome_loc_tit_id";
        $results1 = DB::connection("sqlsrv")->select(
            $sql_sp_GRA_KPD,
            array(
                ':lingua_id' => $lingua_id, //language
                ':nome_loc_tit_id' => (int) (str_replace(" ", "", $nome_loc_tit_id)),
            )
        );
        return $results1;
    }

    function getBizDetails_3(REQUEST $request){
        $lingua_id = '2';
        $nome_loc_tit_id = $request->nome_loc_tit_id;
        $sql_sp_DETALHE_ADICIONAL = "EXEC sp_DETALHE_ADICIONAL "
        . "@LINGUA_ID=:LINGUA_ID, "
        . "@NOME_LOC_TIT_id=:NOME_LOC_TIT_id, "
        . "@CAMPO_ORDENACAO = :CAMPO_ORDENACAO, "
        . "@SENTIDO_ORDENACAO = :SENTIDO_ORDENACAO ";
        $results1 = DB::connection("sqlsrv")->select(
            $sql_sp_DETALHE_ADICIONAL,
            array(
                ':LINGUA_ID' => $lingua_id, //language
                ':NOME_LOC_TIT_id' => (int) (str_replace(" ", "", $nome_loc_tit_id)),
                ':CAMPO_ORDENACAO' => '1',
                ':SENTIDO_ORDENACAO' => 'ASC'
              )
        );
        return $results1;
    }

    /**
     * * Retrieve all the registered branches for a particular business
     * @param type $NOME_LOC_TIT_id ... unique identifier for the business
     * @return type ... associative array collection of all the branches
     */
    function getBizBranches(REQUEST $request)
    {
        $lingua_id = '2';
        $NOME_LOC_TIT_id = $request->NOME_LOC_TIT_id;
        $sql_sp_GETBRANCHES = "EXEC sp_GETBRANCHES @LINGUA_ID=:LINGUA_ID,@NOME_LOC_TIT_id=:NOME_LOC_TIT_id";
        $results = DB::connection("sqlsrv")->select(
            $sql_sp_GETBRANCHES,
            array(
                ':LINGUA_ID' => $lingua_id, //language
                ':NOME_LOC_TIT_id' => (int) (str_replace(" ", "", $NOME_LOC_TIT_id)),
            )
        );
        return $results;
    }


    /**
     * Get the details of a particular business given the business id
     * @param type $id
     * @return type
     */
    function getBizDetailsLite(REQUEST $request)
    {
        $lingua_id = '2';
        $nome_loc_tit_id = $request->nome_loc_tit_id;
        $sql_sp_GRA_KPD = "EXEC sp_GRA_KPD @LINGUA_ID=:lingua_id,@NOME_LOC_TIT_id=:nome_loc_tit_id";
        $results1 = DB::connection("sqlsrv")->select(
            $sql_sp_GRA_KPD,
            array(
                ':lingua_id' => $lingua_id, //language
                ':nome_loc_tit_id' => (int) (str_replace(" ", "", $nome_loc_tit_id)),
            )
        );

        if (empty($results1)) {
            //    $request_uri = !empty(request_uri()) ? request_uri() : "";
            //    watchdog("Business_Details", "NOME_LOC_TIT_id " . (int) (str_replace(" ", "", $nome_loc_tit_id)) . " URI: " . $request_uri);
            return array();
            exit();
        }

        //build logo and advert images
        $logo_url = "https://cdn.yellowpageskenya.com/business-logo.png";
        $lateral_advert = "";
        if ($results1[0]["LOGOTIPO_ID"] !== null) {
            $logo_url = $this->getImgURL($results1[0]["LOGOTIPO_ID"]);
        }
        if ($results1[0]["ANUNCIO_ID"] !== null) {
            $lateral_advert = $this->getImgURL($results1[0]["ANUNCIO_ID"]);
        }
        $images = ["images" => ["logo" => $logo_url, "lateral_advert" => $lateral_advert]];
        //end build logo and advert images

        return array_merge($images, ["details" => $results1[0]]);
    }


    /**
     * Get image linked to a business by an id
     * @param type $logotipo_id ... The id of the image
     * @return type ... relative path to the image resource
     */
    function getImgURL($logotipo_id)
    {
        //  $sql = "select TOP 1 * from KEN_PA_V6.dbo.imagem where IMAGEM_ID = :logotipo_id;";
        $sql = "select TOP 1 * FROM KEN_PA_V6.dbo.IMAGEM_PROCESS where imagem_id = :logotipo_id;";
        $imgdata = DB::connection('sqlsrv')->select($sql, array(':logotipo_id' => $logotipo_id));

        //  return "/sites/default/files/business-images/" . slugify($imgdata->NOME_IMG);
        return "https://cdn.yellowpageskenya.com/" . (!empty($imgdata[0]->url) ? str_replace('\\', '/', $imgdata[0]->url) : "business-logo.png");
    }

    /**
     * Get adverts based on the search query/phrase
     * @param type $phrase
     * @return type ... html markup of the ad images
     */
    function getSidebarImageAds(REQUEST $request)
    {
        $what = $request->what;
        $where = $request->where;
        $category = $request->category;
        $lingua_id = '2';
        $titulo_id = 0;
        global $base_url;

        if ($category !== "") {
            $sql = "SELECT TOP 1 TITULO_ID FROM TITULO  WHERE TITULO = :ref;";
            $results = DB::connection('sqlsrv')->select($sql, array(':ref' => $category));
            $titulo_id = $results[0]->TITULO_ID;
        }

        $sql_sp_GETBOTAO4_KPD = "EXEC sp_GETBOTAO4_KPD "
            . "@pPESQUISA_F = :what, "
            . "@pTitulo = :titulo_id, "
            . "@pLINGUA_ID = :lingua_id";

        $results = DB::connection('sqlsrv')->select(
            $sql_sp_GETBOTAO4_KPD, //get raw additional dataS
            array(
                ':what' => $what . (!empty($where) ? " " . $where : ""),
                ':titulo_id' => $titulo_id, //category id
                ':lingua_id' => $lingua_id, //language
            )
        );

        $theads = array();
        if (!empty($results)) {

            $theads["status"] = TRUE;
            $theads["details"] = [];
            foreach ($results as $ad) {
                $addetails = [];
                $business = $this->getBizDetailsLite($ad['nome_loc_tit_id']);

                $addetails["data-business"] = !empty($business["details"]["bname"]) ? $business["details"]["bname"] : "";
                $addetails["data-catname"] = !empty($business["details"]["title"]) ? $business["details"]["title"] : "";
                $addetails["data-value"] = !empty($business["details"]["bpackage"]) ? $this->getPackageValue($business["details"]["bpackage"]) : 1;

                $addetails["alt"] = !empty($business["details"]["bname"]) ? $business["details"]["bname"] : "";
                $addetails["alt"] .= !empty($business["details"]["locality"]) ? " located at " . $business["details"]["locality"] : "";
                $addetails["alt"] .= !empty($business["details"]["title"]) ? " - " . $business["details"]["title"] : "";
                $addetails["alt"] .= !empty($business["details"]["bmobile"]) ? " phone " . $business["details"]["bmobile"] : "";

                if (!empty($ad["link"]) && (strpos($ad["link"], 'detalhe_pesquisa.aspx?') === FALSE)) {
                    $addetails["link"] = $ad["link"];
                    $addetails["target"] = "_blank";
                } else {
                    $addetails["link"] = '/business-details/' . slugify(check_plain($business["details"]["title"])) . '/' . $ad['nome_loc_tit_id'] . '-' . slugify(check_plain($business["details"]["bname"]));
                    $addetails["target"] = "_self";
                }
                $addetails["image_url"] = getImgURL($ad["imagem_id"]);

                array_push($theads["details"], $addetails);
            }
            return $theads;
        } else {
            $thead["image_url"] = "https://cdn.yellowpageskenya.com/icons/yellow-pages-ad.png";
            $thead["link"] = $base_url . "/contact-us";
            $thead["target"] = "_blank";
            $thead["alt"] = 'Free advertising space Yellow Pages Adspace';
            $theads["status"] = FALSE;
            $theads["details"] = $thead;
            return ($theads);
        }
    }

    /**
     * Get Business Contact details.
     * Params $business_id INT
     * @return \Illuminate\Http\Response
     */
    public function get_business_contact_details(Request $request)
    {
        $business_id = $request->business_id;
        $businesses = DB::connection('sqlsrv')->select('select * from business_contact_details where business_id = ?', [$business_id]);
        // $path = Storage::putFile('json', $request->file('avatar'));
        dd($businesses);
    }

    /**
     * Query all the business categories from the database
     * @return boolean
     */
    function getBusinessCategories()
    {
        $LINGUA_ID = 2;

        $sql_cat = "SELECT DISTINCT([TITULO]) FROM NOME_LOC_TIT "
            . "WHERE [TITULO] !='999999' AND LINGUA_ID=:LINGUA_ID ORDER BY [TITULO] ASC";

        $cats = DB::connection('sqlsrv')->select($sql_cat, array(':LINGUA_ID' => $LINGUA_ID));

        $cats_response = [];
        if (!empty($cats)) {
            $cats_response["status"] = true;
            $cats_response["total_results"] = sizeof($cats);
            $cats_response["results"] = $cats;
        } else {
            $cats_response["status"] = false;
        }
        return $cats_response;
    }

    /**
     * Given a category ID, get its name
     * @param type $category ... the category id
     */
    function getCategoryName($CAT_ID = 0)
    {
        $LINGUA_ID = 2;
        $CAT_NAME = "";
        $sql_titulo = 'SELECT TOP 1 TITULO_ID, TITULO FROM TITULO WHERE TITULO_ID = :CAT_ID;';
        $data = DB::connection('sqlsrv')->select(
            $sql_titulo,
            array(
                ':CAT_ID' => $CAT_ID
            )
        );
        if (!empty($data)) :
            $CAT_NAME = $data["TITULO"];
        endif;
        return $CAT_NAME;
    }

    /**
     * Get unique categories and locations from search results array
     * @param type $results
     * @return type
     */
    function getCatLoc($results)
    {

        $cats = array();
        $locs = array();
        $key_words = array();

        foreach ($results as $result) {
            $cats[] = trim($result['title']);
            $locs[] = trim($result['region']);
            $locs[] = trim($result['region']) !== trim($result['locality']) ?
                trim($result['region']) . " - " . trim($result['locality']) :
                "";
            $key_words[] = $result['title'];
            $key_words[] = $result['bname'];
        }

        $getCatLocKeywords = [
            "cats" => array_unique($cats),
            "locations" => array_unique($locs),
            "keywords" => array_unique($key_words)
        ];

        return $getCatLocKeywords;
    }

    /**
     * Given a category name, get its unique identifier
     * @param type $category ... the category name
     * @return type ... int the category id
     */
    function getCategoryIdbyName()
    {
        $LINGUA_ID = 2;
        $params = drupal_get_query_parameters(); // import to cms functions
        $CAT_NAME = "";
        $CAT_ID = 0;
        if (isset($params["category"])) {
            $CAT_NAME = $params["category"];
            $sql_titulo = 'SELECT TOP 1 TITULO_ID, TITULO FROM TITULO WHERE TITULO = :CAT_NAME;';
            $data = DB::connection('sqlsrv')->select(
                $sql_titulo,
                array(
                    ':CAT_NAME' => $CAT_NAME
                )
            );
            if (!empty($data)) :
                $CAT_ID = $data["TITULO_ID"];
            endif;
        }
        return (int) $CAT_ID;
    }

    /**
     * Given a category name, get its unique identifier
     * @param type $category ... the category name
     * @return type ... int the category id
     */
    function getCategoryIdbyName2($category)
    {
        $LINGUA_ID = 2;
        $CAT_NAME = "";
        $CAT_ID = 0;

        if (!empty($category)) :
            $sql_titulo = 'SELECT TOP 1 TITULO_ID, TITULO FROM TITULO WHERE TITULO = :CAT_NAME;';
            $data = DB::connection('sqlsrv')->select(
                $sql_titulo,
                array(
                    ':CAT_NAME' => (string) $category
                )
            );
            if (!empty($data)) :
                $CAT_ID = $data["TITULO_ID"];
            endif;
        endif;
        return (int) $CAT_ID;
    }


    function cleanSP_LINHAS($results_sp_LINHAS, $additional_contacts)
    {

        $contacts = [];
        $results3 = [
            'contacts' => null,
            'general_info' => null,
            'po_box' => null,
            'brands' => null,
            'products' => null,
            'services' => [],
            'hours' => null,
            'payment' => null,
            'handle_fb' => null,
            'handle_tweet' => null,
            'handle_instagram' => null,
            'latlon' => null
        ];

        //  process additional concacts
        if (!empty($additional_contacts)) {
            foreach ($additional_contacts as $contact) {
                if ($contact["TIPO_MEIO"] == "Telephone") {
                    array_push($contacts, $contact["MEIO"]);
                }
                if ($contact["TIPO_MEIO"] == "Mobile") {
                    array_push($contacts, $contact["MEIO"]);
                }
            }
        }

        if (!empty($results_sp_LINHAS)) {
            foreach ($results_sp_LINHAS as $result) {
                //GENERAL INFO / ABOUT BUSINESS
                $gen_info_codes = ["O_DCW", "IP_DESC_2"];
                if (in_array($result['COD_LINHA'], $gen_info_codes)) {
                    $results3['general_info'] = $result['TEXTO'];
                }
                //PO BOX
                if ($result['COD_LINHA'] == 'O_BOX') {
                    $results3['po_box'] = $result['TEXTO'];
                }

                //BEGIN SERVICES
                $svc_codes = ['O_SERV1', 'O_SERV2', 'O_PRD4', "IP_SERV_2"];
                if (in_array($result['COD_LINHA'], $svc_codes)) {
                    array_push($results3['services'], $result['TEXTO']);
                }

                //BRANDS
                if ($result['COD_LINHA'] == 'O_BR') {
                    $results3['brands'] .= $result['TEXTO'];
                }

                //PRODUCTS
                $prod_codes = ['O_PRD1', 'O_SERV4', 'IP_PROD_2'];
                if (in_array($result['COD_LINHA'], $prod_codes)) {
                    $results3['products'] .= $result['TEXTO'];
                }

                //METHODS OF PAYMENT
                if ($result['COD_LINHA'] == 'O_MP') { //Methods of payment
                    $results3['payment'] .= $result['TEXTO'];
                }

                //OPENING AND CLOSING HOURS
                if ($result['COD_LINHA'] == 'O_OPH') { //Opening and closing hours
                    $results3['hours'] .= $result['TEXTO'];
                }

                //MAP LATITUDE AND LOGITUDE
                if ($result['COD_LINHA'] == 'O_MAP') { //map latitude longitude
                    $results3['latlon'] = $result['TEXTO'];
                }

                //TELEPHONE
                if ($result['COD_LINHA'] == 'O_TL') { //Telephone
                    array_push($contacts, $result['TEXTO']);
                }

                //MOBILE NUMBER
                if ($result['COD_LINHA'] == 'O_ML') { //Mobile number
                    array_push($contacts, $result['TEXTO']);
                }
            }
        }

        $results3['contacts'] = array_unique($contacts);

        return $results3;
    }


    /**
     * Given the required params,
     * determine if the execution of the query will yield new results
     *
     * @param type $lingua_id
     * @param type $what
     * @param type $where
     * @param type $location_id
     * @param type $FilterLocality_F
     * @param type $classTitle_ID
     * @return type
     */
    function getCountSearchResults(REQUEST $request)
    {
        $lingua_id = $request->lingua_id;
        $what = $request->what;
        $where = $request->where;
        $location_id = $request->location_id;
        $FilterLocality_F = $request->FilterLocality_F;
        $classTitle_ID = $request->classTitle_ID;

        $count_total_results = DB::connection('sqlsrv')->select(
            "EXEC sp_PAI_PESQUISA_V8 "
                . "@LINGUA_ID=:lang, "
                . "@PESQUISA_F=:what, "
                . "@LOCALITY_F=:where, "
                . "@LOCALIDADE_ID=:location_id, "
                . "@FilterLocality_F = :FilterLocality_F,  "
                . "@TITULO_ID =:classtitle, "
                . "@PCOUNT = 1;",
            array(
                ':lang' => $lingua_id, //language
                ':what' => $what,
                ':where' => $where,
                ':location_id' => $location_id,
                ':FilterLocality_F' => $FilterLocality_F,
                ':classtitle' => $classTitle_ID,
            )
        );
        $results = (array)$count_total_results[0];
        $count = 0;
        foreach ($results as $result) :
            $count = $result;
        endforeach;
        return response()->json(['count' => $count, 'status' => 'ok'], 200);
    }

    /**
     * Fetch the actual results
     *
     * @param type $lingua_id
     * @param type $what
     * @param type $where
     * @param type $location_id
     * @param type $FilterLocality_F
     * @param type $classTitle_ID
     * @param type $current_page
     * @param type $per_page
     * @return type
     */
    function getExecSearchResults(REQUEST $request)
    {
        $lingua_id = $request->lingua_id;
        $what = $request->what;
        $where = $request->where;
        $location_id = $request->location_id;
        $FilterLocality_F = $request->FilterLocality_F;
        $classTitle_ID = $request->classTitle_ID;
        $cp = $request->current_page;
        $pp = $request->per_page;

        $startposition = ($cp * $pp) + 1;
        $results = DB::connection('sqlsrv')->select(
            "EXEC sp_PAI_PESQUISA_V8 "
                . "@LINGUA_ID=:lang, "
                . "@PESQUISA_F=:what, "
                . "@LOCALITY_F=:where, "
                . "@LOCALIDADE_ID=:location_id, "
                . "@FilterLocality_F = :FilterLocality_F,  "
                . "@TITULO_ID =:classtitle, "
                . "@PINITIAL =:start_position,  "
                . "@PREG = :per_page;",
            array(
                ':lang' => $lingua_id, //language
                ':what' => $what,
                ':where' => $where,
                ':location_id' => $location_id,
                ':FilterLocality_F' => $FilterLocality_F,
                ':classtitle' => $classTitle_ID,
                ':start_position' => $startposition,
                ':per_page' => $pp,
            )
        );
        return $results;
    }

    /**
     * get the package value of business category for tracking
     * @param type $package
     * @return int
     */
    function getPackageValue($package)
    {
        $value = 1;
        switch ($package) {
            case "O_JADE":
                $value = 10;
                break;

            case "O_PEARL":
                $value = 9;
                break;

            case "O_EMERALD":
                $value = 8;
                break;

            case "O_DIAMOND":
                $value = 7;
                break;

            case "O_PLATINUM":
                $value = 6;
                break;

            case "O_GOLD":
                $value = 5;
                break;

            case "O_SILVER":
                $value = 4;
                break;

            case "O_BRONZE":
                $value = 3;
                break;

            case "O_BASIC":
                $value = 2;
                break;

            default:
                $value = 1;
                break;
        }

        return $value;
    }
}
